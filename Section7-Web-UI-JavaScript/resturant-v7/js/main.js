/*
Requirements:
1. When the user submits the Contact Us form, use JavaScript to check:
    a. Name and one form of contact information (Email or Phone) should be filled in.
    b. If Reason for Inquiry’s dropdown is selected to Other, make sure that the Additional
    Information is filled in.
    c. Best days to contact you must have at least one day checked.
2. If any of these are blank, display a pop-up to politely remind the user to fill in these fields
*/

var myNodeName;
var myNodeTel;
var myNodeEmail;
var myNodeOtherSelect;
var myNodeOtherValue;
var myNodeAddDescValue;
var myNodeCheckBox;
var days = [];

myNodeButtonType = document.getElementById("buttonTypeChange");
/////////////// SUBMIT BUTTON TYPE /////




myNodeButtonType.addEventListener('click', function () {
    myNodeName = document.getElementById("name").value;
    myNodeTel = document.getElementById("tel").value;
    myNodeEmail = document.getElementById("email").value;
    myNodeOtherSelect = document.getElementById("other").selected;
    myNodeOtherValue = document.getElementById("other").value;
    myNodeAddDescValue = document.getElementById("briefDesc").value;
    myNodeCheckBox = document.getElementsByClassName("checkBox");


    //////////testing - debugging purposes /////
    console.log(myNodeName);
    // returns userInput value
    console.log(myNodeEmail);
    // returns userInput value
    console.log(myNodeTel);
    // returns userInput value
    console.log(myNodeOtherSelect);
    //returns boolean
    console.log(myNodeOtherValue);
    //returns value
    console.log(myNodeAddDescValue);
    // returns value
    console.log(myNodeCheckBox);
    //returns an array

    ////////////////////end debug /////////////////
    // use a switch statement /////

    if (myNodeName == "") {
        alert("ENTER YOUR NAME");
    }
    if (myNodeEmail == "" && myNodeTel == "") {
        alert("Enter one form of contact: EMAIL or PHONE");
    }
    if (myNodeAddDescValue != "") {
        myNodeOtherSelect = false;
    }
    if (myNodeOtherSelect == true) {
        alert("You Selected " + myNodeOtherValue.toUpperCase() + " , Could you give us additional information, thank you");
    }
});

myNodeButtonType.addEventListener('click', function () {
    for (var i = 0; i < myNodeCheckBox.length; i++) {
        if (myNodeCheckBox[i].checked == true) {
            console.log(myNodeCheckBox[i].checked);
            days.push(myNodeCheckBox[i].value);
            //returns array of days
        }
    }
    if (days < 1) {
        alert("We Need at least one DAY Checked to contact you, THANK YOU!");
    } else if (myNodeName != "" && myNodeOtherSelect != true && (myNodeEmail != "" || myNodeTel != "")) {
        myNodeButtonType.type = "submit";
        alert("Great We will be contacting you on, ".toLocaleUpperCase() + days + " ,Thank You!".toUpperCase());
    }
});
