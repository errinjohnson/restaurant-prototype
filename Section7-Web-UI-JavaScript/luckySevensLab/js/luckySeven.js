console.log("start"); // START DEBUGGING LOG
/*
1. The game should be laid out like this:
    a. Note: display: input, play button
    b. Note: hidden: #results, #playAgain button
*/
//input area and submit button;
//name="betValue"
/*
2. The program asks the user how many dollars they have to bet.
*/
var userNumberBalance;
/*
Set global variables
    a. Hint: For steps 4, 5, and 6, declare some variables.
*/

var rolls = [];
var maxAmt = [];
var maxRoll = [];

//Start - roll func //
function roll(dice1, dice2) {
    var dice1 = Math.floor((Math.random() * 6) + 1);
    var dice2 = Math.floor((Math.random() * 6) + 1);
    var rolling = dice1 + dice2;
    return rolling;
}
// End - roll func //

/*
3. When the user clicks the Play button, the program then rolls the dice repeatedly until all the money is gone.
    a. Inialize Play() function on html #play div
4. The program keeps track of how many rolls were taken before the money ran out.
    b. Intialize roll();
5. The program keeps track of the maximum amount of money held by the player.
6. The program keeps track of how many rolls were taken at the point when the user held the most money.
    c. push results to arrays
*/

// Start Play func //
function play() {
    userNumberBalance = document.getElementById("bets").value;
    document.getElementById("startBet").innerHTML = userNumberBalance;
    for (var j = 0; userNumberBalance >= 1; j++) {
        console.log("$" + userNumberBalance + " balance");
        var num = roll();
        if (num == 7) {
            userNumberBalance += 4;
            maxAmt.push(userNumberBalance);
            rolls.push(j);
            console.log("player wins " + "rolled a " + num);
        } else {
            userNumberBalance -= 1;
            maxAmt.push(userNumberBalance);
            rolls.push(j);
            console.log("player loses " + "rolled a " + num);
        }
    }

    display1();
    /*
    maxAmt array [] and maxIndex array [] set
    */
    var max = Math.max(...maxAmt);
    var maxIndex = maxAmt.indexOf(max);

    //DISPLAY RESULTS VALUES IN HTML TABLE #results div //
    document.getElementById("totalRolls").innerHTML = rolls.length;
    document.getElementById("max").innerHTML = "$" + max;
    document.getElementById("maxIndex").innerHTML = maxIndex;
    //DEBUGGING LOG//
    console.log(userNumberBalance + " balance");
    console.log(rolls.length);
    console.log(rolls);
    console.log(maxAmt);
    console.log(max);
    console.log(maxIndex);
    console.log("finish");
    //END OF DEBUGGING LOG//
}
//End Play func //

/*
7. When the game is over, display the following output:
    a. Note: Play button html = type: "button", so form will NOT reset.
*/
function display1() {
    document.getElementById("play").style.display = "none";
    document.getElementById("results").style.display = "block";
    document.getElementById("playAgain").style.display = "block";
    document.getElementById("note").innerHTML = "NOTE: See web console Ctrl Shift i in Chrome browser for testing results";
}
/*
display: #results
hides: play button
a. Note: Give your user the opportunity to play again
    b. Note: playAgain button is set to TYPE:SUBMIT..
        resets form...
            HIDDEN ELEMENTS (CSS FILE)RESET UNTIL PLAY BUTTON IS HIT ETC
*/
